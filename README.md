# CS Games 2017 - Security

An already built Docker image of the Security challenge of the 2017
edition of the CS Games.

Simply run:

```
docker run -d registry.gitlab.com/ageei/ctf/csgames17-security
# note the hash to get the container ip
docker inspect $hash | grep '"IPAddress"'
```

## Changes

In order to make the Docker image build, we pinned the `ubuntu`
source image to `xenial` -- which was the version used at the time.

## Source

[https://github.com/ldionmarcial/CSG2017-security](https://github.com/ldionmarcial/CSG2017-security)
